package practica7;

import java.util.Iterator;
import util.Protocol_base;
import util.TCPSegment;
import util.SimNet;
import util.TSocket_base;

public class Protocol extends Protocol_base {

    protected Protocol(SimNet net) {
        super(net);
    }

    public void ipInput(TCPSegment segment) {
        int srcPort = segment.getSourcePort();
        int destPort = segment.getDestinationPort();
        TSocket_base socket = getMatchingTSocket(destPort, srcPort);
        if (socket != null) {
            socket.processReceivedSegment(segment);
            log.printGREEN("Success "+destPort+" "+srcPort);
        } else {
            log.printRED("Error "+destPort+" "+srcPort);
        }
    }

    protected TSocket_base getMatchingTSocket(int localPort, int remotePort) {
        lk.lock();
        try {
            log.printRED("Connecting "+remotePort+" "+localPort);
            Iterator<TSocket_base> it = activeSockets.iterator();
            while (it.hasNext()) {
                TSocket_base socket = it.next();
                if (socket.localPort == localPort && socket.remotePort == remotePort) {
                    return socket;
                }
            }
            it = listenSockets.iterator();
            while (it.hasNext()) {
                TSocket_base socket = it.next();
                if (socket.localPort == localPort) {
                    listenSockets.get(listenSockets.indexOf(socket));
                    activeSockets.add(socket);
                    return socket;
                }
            }
            return null; // No es troba cap socket amb els ports especificats.
        } finally {
            lk.unlock();
        }
    }

}
