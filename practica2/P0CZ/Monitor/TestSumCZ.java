package practica2.P0CZ.Monitor;

public class TestSumCZ {

    public static void main(String[] args) throws InterruptedException {
        MonitorCZ monitor = new MonitorCZ();
        
        Thread t1 = new Thread(new CounterThreadCZ(monitor));
        Thread t2 = new Thread(new CounterThreadCZ(monitor));
        
        t1.start();
        t2.start();
        
        t1.join();
        t2.join();
        
        System.out.println(monitor.getX());
    }
}
