package practica2.P0CZ.Monitor;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MonitorCZ {

    private int x = 0;
    private Lock lock = new ReentrantLock();
    
    public void inc() {
        //Incrementa en una unitat el valor d'x
        lock.lock();
        x++;
        lock.unlock();
    }

    public int getX() {
        //Ha de retornar el valor d'x
        return x;
    }

}
