package practica2.P0CZ;

public class TestSum {

    public static void main(String[] args) throws InterruptedException {

        Thread t1 = new Thread(new CounterThread());
        Thread t2 = new Thread(new CounterThread());
        
        t1.start();
        t2.start();
        
        t1.join();
        t2.join();
        
        System.out.println(CounterThread.x);
    }
}
