package practica2.P1Sync.Monitor;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MonitorSync {

    private final int N;
    private Lock lock = new ReentrantLock();
    private Condition esperar = lock.newCondition();
    private int torn;

    public MonitorSync(int N) {
        this.N = N;
    }

    public void waitForTurn(int id) {
        lock.lock();
        while (torn != id) {
            try {
                esperar.await();
            } catch (InterruptedException ex) {
            }
        }
    }

    public void transferTurn() {
        torn = (torn + 1) % N;
        esperar.signalAll();
        lock.unlock();
    }
}
