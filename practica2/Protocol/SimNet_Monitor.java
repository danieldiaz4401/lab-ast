package practica2.Protocol;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import practica1.CircularQ.CircularQueue;
import util.Const;
import util.TCPSegment;
import util.SimNet;

public class SimNet_Monitor implements SimNet {

    protected CircularQueue<TCPSegment> queue;
    private Lock lock = new ReentrantLock();
    private Condition isFull;
    private Condition isEmpty;

    public SimNet_Monitor() {
        queue = new CircularQueue<>(Const.SIMNET_QUEUE_SIZE);
        isFull = lock.newCondition();
        isEmpty = lock.newCondition();
    }

    @Override
    public void send(TCPSegment seg) {
        lock.lock();
        while (queue.full()) {
            try {
                isFull.await();
            } catch (InterruptedException ex) {
            }
        }
        queue.put(seg);
        isEmpty.signal();
        lock.unlock();
    }

    @Override
    public TCPSegment receive() {
        lock.lock();
        while (queue.empty()) {
            try {
                isEmpty.await();
            } catch (InterruptedException ex) {
            }
        }
        isFull.signal();
        TCPSegment value = (TCPSegment) queue.get();
        lock.unlock();
        return value;
    }

    @Override
    public int getMTU() {
        throw new UnsupportedOperationException("Not supported yet. NO cal completar...");
    }

}
