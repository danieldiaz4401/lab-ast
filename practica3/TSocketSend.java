package practica3;

import java.util.Arrays;
import util.Const;
import util.TCPSegment;
import util.TSocket_base;
import util.SimNet;

public class TSocketSend extends TSocket_base {

    protected int MSS;       // Maximum Segment Size

    public TSocketSend(SimNet net) {
        super(net);
        MSS = net.getMTU() - Const.IP_HEADER - Const.TCP_HEADER;
    }

    @Override
    public void sendData(byte[] data, int offset, int length) {
        for (int i = 0; i < length; i = i + MSS) {
            int util = Math.min(MSS, length - i);
            TCPSegment segment = segmentize(data, offset + i, util);
            this.network.send(segment);
            System.out.println("snd --> " + segment.toString());
        }
    }

    protected TCPSegment segmentize(byte[] data, int offset, int length) {
        TCPSegment seg = new TCPSegment();
        seg.setData(data, offset, length);
        seg.setPsh(true);
        return seg;
    }

}
