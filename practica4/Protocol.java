package practica4;

import java.util.Iterator;
import util.Protocol_base;
import util.TCPSegment;
import util.SimNet;
import util.TSocket_base;

public class Protocol extends Protocol_base {

    public Protocol(SimNet net) {
        super(net);
    }

    protected void ipInput(TCPSegment seg) {
        int srcPort = seg.getSourcePort();
        int destPort = seg.getDestinationPort();
        TSocket_base socket = getMatchingTSocket(destPort, srcPort);
        if (socket != null) {
            socket.processReceivedSegment(seg);
        }else{
            System.err.println("No s'ha trobat cap port:\n\tsrcPort: "+srcPort+"\n\tdestPort: "+destPort);
        }
    }

    protected TSocket_base getMatchingTSocket(int localPort, int remotePort) {
        lk.lock();
        try {
            Iterator<TSocket_base> it = activeSockets.iterator();
            while (it.hasNext()) {
                TSocket_base socket = it.next();
                if (socket.localPort == localPort && socket.remotePort == remotePort) {
                    return socket;
                }
            }
            return null; // No es troba cap socket amb els ports especificats.
        } finally {
            lk.unlock();
        }
    }
}
