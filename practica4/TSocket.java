package practica4;

import practica1.CircularQ.CircularQueue;
import util.Const;
import util.TCPSegment;
import util.TSocket_base;

public class TSocket extends TSocket_base {

    //sender variable:
    protected int MSS;

    //receiver variables:
    protected CircularQueue<TCPSegment> rcvQueue;
    protected int rcvSegConsumedBytes;

    protected TSocket(Protocol p, int localPort, int remotePort) {
        super(p.getNetwork());
        this.localPort = localPort;
        this.remotePort = remotePort;
        p.addActiveTSocket(this);
        MSS = network.getMTU() - Const.IP_HEADER - Const.TCP_HEADER;
        rcvQueue = new CircularQueue<>(Const.RCV_QUEUE_SIZE);
        rcvSegConsumedBytes = 0;
    }

    @Override
    public void sendData(byte[] data, int offset, int length) {
        // Copiat de P3
        for (int i = 0; i < length; i = i + MSS) {
            int util = Math.min(MSS, length - i);
            TCPSegment segment = segmentize(data, offset + i, util);
            this.network.send(segment);
        }
        //
    }

    protected TCPSegment segmentize(byte[] data, int offset, int length) {
        // Copiat de P3
        TCPSegment seg = new TCPSegment();
        seg.setData(data, offset, length);
        seg.setPsh(true);
        seg.setSourcePort(localPort);
        seg.setDestinationPort(remotePort);
        return seg;
        //
    }

    @Override
    public int receiveData(byte[] buf, int offset, int length) {
        lock.lock();
        try {
            // Copiat de P3
            int agafats = 0;
            while (rcvQueue.empty()) {
                try {
                    super.appCV.await();
                } catch (InterruptedException ex) {
                }
            }
            do {
                agafats += consumeSegment(buf, offset + agafats, length - agafats);
            } while (agafats < length && !rcvQueue.empty());
            return agafats;
            //
        } finally {
            lock.unlock();
        }
    }

    protected int consumeSegment(byte[] buf, int offset, int length) {
        TCPSegment seg = rcvQueue.peekFirst();
        int a_agafar = Math.min(length, seg.getDataLength() - rcvSegConsumedBytes);
        System.arraycopy(seg.getData(), rcvSegConsumedBytes, buf, offset, a_agafar);
        rcvSegConsumedBytes += a_agafar;
        if (rcvSegConsumedBytes == seg.getDataLength()) {
            rcvQueue.get();
            rcvSegConsumedBytes = 0;
        }
        return a_agafar;
    }

    protected void sendAck() {
        TCPSegment segment = new TCPSegment();
        segment.setAck(true);
        segment.setSourcePort(localPort);
        segment.setDestinationPort(remotePort);
        this.network.send(segment);
    }

    @Override
    public void processReceivedSegment(TCPSegment rseg) {
        lock.lock();
        try {
            printRcvSeg(rseg);
            if (rseg.isAck()) {
                //nothing to be done in this exercise.
            } else if (!rcvQueue.full()) {
                rcvQueue.put(rseg);
                sendAck();
                super.appCV.signalAll();
            } else {
                System.out.println("Descartat: " + rseg.toString());
            }
        } finally {
            lock.unlock();
        }
    }

}
