package practica1.CircularQ;

import java.util.Iterator;
import java.util.NoSuchElementException;
import util.Queue;

public class CircularQueue<E> implements Queue<E> {

    private final E[] queue;
    private final int N;
    private int primer; // Primera posició ocupada
    private int mida; // Ocupació 

    public CircularQueue(int N) {
        this.N = N;
        this.queue = (E[]) (new Object[N]);
        this.primer = 0;
        this.mida = 0;
    }

    // Funció privada que retorna la posició d'inserció
    private int insercio() {
        return (this.primer + this.mida) % this.N;
    }

    // Funció privada que retorna la última posició
    private int ultim() {
        return this.insercio() - 1;
    }

    @Override
    public int size() {
        return mida;
    }

    @Override
    public int free() {
        return N - this.mida; // L'espai lliure és l'espai total menys l'ocupat
    }

    @Override
    public boolean empty() {
        return this.mida == 0; // No totes les posicions estan ocupades 
    }

    @Override
    public boolean full() {
        return this.mida == N; // Totes les posicions estan ocupades 
    }

    @Override
    public E peekFirst() {
        if (this.empty()) { // Si la llista esta buida retorna null
            return null;
        }
        return this.queue[this.primer]; // Retorna, però no elimina, el primer element de la cua
    }

    @Override
    public E get() {
        if (this.empty()) {
            throw new IllegalStateException("Cua buida"); // Si la cua està buida, llença una IllegalStateException
        }
        E e = this.queue[this.primer];
        this.primer = (this.primer + 1) % N;
        this.mida--;
        return e; // Retorna i elimina el primer element de la cua
    }

    @Override
    public void put(E e) {
        if (this.full()) {
            throw new IllegalStateException("Cua plena"); // Si la cua està plena, llença una IllegalStateException
        }
        this.queue[this.insercio()] = e; // Afageix a la posició següent l'element
        this.mida++;
    }

    @Override
    public String toString() {
        String result = "[";
        Iterator<E> it = this.iterator();
        while (it.hasNext()) {
            result += it.next().toString();
            if (it.hasNext()) {
                result += ", ";
            }
        }
        return result + "]";
    }

    @Override
    public Iterator<E> iterator() {
        return new MyIterator();
    }

    class MyIterator implements Iterator {

        private int cursor;
        private final int INICI;
        private boolean start;

        public MyIterator() {
            this.INICI = (primer + N - 1) % N;
            this.cursor = 0;
            this.start = true;
        }

        @Override
        public boolean hasNext() {
            return this.cursor < mida; // No poso != per evitar el cas on n=0 
        }

        @Override
        public E next() {
            if (this.start) {
                this.start = false;
            }
            if (!this.hasNext()) {
                throw new NoSuchElementException("La posició " + (this.cursor + primer) + " està fora de la cua");
            }
            this.cursor++;
            return queue[(this.INICI + this.cursor) % N];
        }

        @Override
        public void remove() {
            if (this.start) {
                throw new IllegalStateException("No hi ha cap element per eliminar");
            }
            if (mida == 0) {
                throw new IllegalStateException("No hi queden elements per eliminar");
            }
            for (int i = cursor; i < mida; i++) {
                queue[(this.INICI + i) % N] = queue[(this.INICI + i + 1) % N];
            }
            cursor = (cursor + N - 1) % N;
            mida--;
        }
    }
}
