package practica1.LinkedQ;

import java.util.Iterator;
import java.util.NoSuchElementException;
import util.Queue;

public class LinkedQueue<E> implements Queue<E> {

    private Node<E> primer;
    private Node<E> ultim;
    private int size;

    // Podria obivar el constructor
    public LinkedQueue() {
        this.primer = null;
        this.ultim = null;
        this.size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int free() {
        throw new UnsupportedOperationException("Not applies");
    }

    @Override
    public boolean empty() {
        return this.size == 0;
    }

    @Override
    public boolean full() {
        return false;
    }

    @Override
    public E peekFirst() {
        return (E) this.primer.getValue();
    }

    @Override
    public E get() {
        if (size == 0) {
            throw new IllegalStateException("Cua plena"); // Si la cua està plena, llença una IllegalStateException
        }
        Node valor = primer;
        primer = primer.getNext();
        if (primer == null) {
            ultim = null;
        }
        size--;
        return (E) valor.getValue();
    }

    @Override
    public void put(E e) {
        Node node = new Node();
        node.setValue(e);
        if (size == 0) {
            primer = node;
            ultim = node;
        } else {
            ultim.setNext(node);
            ultim = node;
        }
        size++;
    }

    @Override
    public String toString() {
        String result = "[";
        Iterator<E> it = this.iterator();
        while (it.hasNext()) {
            result += it.next().toString();
            if (it.hasNext()) {
                result += ", ";
            }
        }
        return result + "]";
    }

    @Override
    public Iterator<E> iterator() {
        return new MyIterator();
    }

    class MyIterator implements Iterator {

        private Node cursor;
        private Node anterior;
        private boolean start;

        public MyIterator() {
            cursor = new Node(); // En el moment q es prem next per primer cop retorna el primer valor
            cursor.setNext(primer);
            anterior = null;
            start = true;
        }

        @Override
        public boolean hasNext() {
            return cursor.getNext() != null;
        }

        @Override
        public E next() {
            start = false;
            if (!this.hasNext()) {
                throw new NoSuchElementException("La posició està fora de la cua");
            }
            anterior = cursor;
            cursor = cursor.getNext();
            return (E) cursor.getValue();
        }

        @Override
        public void remove() {
            if (start) {
                throw new IllegalStateException("No hi ha cap element per eliminar");
            }
            if (size == 0) {
                throw new IllegalStateException("No hi queden elements per eliminar");
            }
            if (!hasNext()) {
                anterior.setNext(null);
                cursor = anterior;
                if (size == 1) {
                    primer = null;
                    ultim = primer;
                } else {
                    ultim = anterior;
                }
            } else if (cursor == primer) {
                primer = cursor.getNext();
                anterior.setNext(primer);
                cursor = anterior;
            } else {
                anterior.setNext(cursor);
                cursor = anterior;
            }
            size--;
        }
    }
}
