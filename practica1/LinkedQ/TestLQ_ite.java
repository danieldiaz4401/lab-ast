package practica1.LinkedQ;

import java.util.Iterator;

public class TestLQ_ite {

    public static void main(String[] args) {

        LinkedQueue<Integer> q = new LinkedQueue<>();
        for (int i = 0; i < 11; i++) {
            q.put(i);
        }
        System.out.println("Queue content: " + q);

        Iterator<Integer> ite = q.iterator();

        System.out.println("we iterate over the queue elements to take 4 and 6, if present:");
        while (ite.hasNext()) {
            int valor = ite.next();
            if (valor <= 10) {
                ite.remove();
                System.out.println("taken: " + valor);
            }
            System.out.println("present content of the queue: " + q);
        }
        System.out.println("present content of the queue: " + q);
    }
}
