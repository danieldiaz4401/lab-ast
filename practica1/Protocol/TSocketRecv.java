package practica1.Protocol;

import util.TCPSegment;
import util.TSocket_base;
import util.SimNet;

public class TSocketRecv extends TSocket_base {

    public TSocketRecv(SimNet net) {
        super(net);
    }

    @Override
    public int receiveData(byte[] data, int offset, int length) {
        try {
            TCPSegment segmentRebut = this.network.receive();
            byte[] arrayRebut = segmentRebut.getData();
            int bytesEnviats = 0;
            for (int i = 0; i < length && i < arrayRebut.length; i++, bytesEnviats++) {
                data[offset + i] = arrayRebut[i];
            }
            printRcvSeg(segmentRebut);
            return bytesEnviats;
        } catch (Exception e) {
            throw e;
        }
    }
}
