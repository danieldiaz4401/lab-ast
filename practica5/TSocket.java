package practica5;

import practica1.CircularQ.CircularQueue;
import practica4.Protocol;
import util.Const;
import util.TSocket_base;
import util.TCPSegment;

public class TSocket extends TSocket_base {

    // Sender variables:
    protected int MSS;
    protected int snd_sndNxt;
    protected int snd_rcvWnd;
    protected int snd_rcvNxt;
    protected TCPSegment snd_UnacknowledgedSeg;
    protected boolean zero_wnd_probe_ON;

    // Receiver variables:
    protected CircularQueue<TCPSegment> rcv_Queue;
    protected int rcv_SegConsumedBytes;
    protected int rcv_rcvNxt;

    protected TSocket(Protocol p, int localPort, int remotePort) {
        super(p.getNetwork());
        this.localPort = localPort;
        this.remotePort = remotePort;
        p.addActiveTSocket(this);
        // init sender variables
        MSS = p.getNetwork().getMTU() - Const.IP_HEADER - Const.TCP_HEADER;
        // init receiver variables
        //rcv_Queue = new CircularQueue<>(Const.RCV_QUEUE_SIZE);
        snd_rcvWnd = Const.RCV_QUEUE_SIZE;
        rcv_Queue = new CircularQueue<>(2);
    }

    // -------------  SENDER PART  ---------------
    @Override
    public void sendData(byte[] data, int offset, int length) {
        lock.lock();
        try {
            int bytesEnviats = 0;
            while (bytesEnviats < length) {
                while (snd_sndNxt - snd_rcvNxt >= 1 || zero_wnd_probe_ON) {
                    super.appCV.await();
                }
                int util = 0;
                TCPSegment seg;
                if (snd_rcvWnd > 0) {
                    util = Math.min(MSS, length - bytesEnviats);
                } else {
                    zero_wnd_probe_ON = true;
                    log.printBLUE("---ZERO WND PROBE ON---");
                    util = 1;
                }
                seg = segmentize(data, offset + bytesEnviats, util);
                bytesEnviats += util;
                snd_UnacknowledgedSeg = seg;
                network.send(seg);
                startRTO();
                snd_sndNxt++;
            }
        } catch (InterruptedException ex) {
        } finally {
            lock.unlock();
        }
    }

    protected TCPSegment segmentize(byte[] data, int offset, int length) {
        // Copiat de P4
        TCPSegment seg = new TCPSegment();
        seg.setData(data, offset, length);
        seg.setPsh(true);
        seg.setSourcePort(localPort);
        seg.setDestinationPort(remotePort);
        seg.setSeqNum(snd_rcvNxt);
        return seg;
        //
    }

    @Override
    protected void timeout() {
        lock.lock();
        try {
            if (snd_UnacknowledgedSeg != null) {
                if (zero_wnd_probe_ON) {
                    log.printBLUE("0-wnd probe: " + snd_UnacknowledgedSeg);
                } else {
                    log.printPURPLE("retrans: " + snd_UnacknowledgedSeg);
                }
                network.send(snd_UnacknowledgedSeg);
                startRTO();
            }
        } finally {
            lock.unlock();
        }
    }

    // -------------  RECEIVER PART  ---------------
    @Override
    public int receiveData(byte[] buf, int offset, int maxlen) {
        lock.lock();
        try {
            // Copiat de P3
            int agafats = 0;
            while (rcv_Queue.empty()) {
                try {
                    super.appCV.await();
                } catch (InterruptedException ex) {
                }
            }
            do {
                agafats += consumeSegment(buf, offset + agafats, maxlen - agafats);
            } while (agafats < maxlen && !rcv_Queue.empty());
            return agafats;
            //
        } finally {
            lock.unlock();
        }
    }

    protected int consumeSegment(byte[] buf, int offset, int length) {
        TCPSegment seg = rcv_Queue.peekFirst();
        int a_agafar = Math.min(length, seg.getDataLength() - rcv_SegConsumedBytes);
        System.arraycopy(seg.getData(), rcv_SegConsumedBytes, buf, offset, a_agafar);
        rcv_SegConsumedBytes += a_agafar;
        if (rcv_SegConsumedBytes == seg.getDataLength()) {
            rcv_Queue.get();
            rcv_SegConsumedBytes = 0;
        }
        return a_agafar;
    }

    protected void sendAck() {
        TCPSegment ack = new TCPSegment();
        ack.setAck(true);
        ack.setAckNum(rcv_rcvNxt);
        ack.setSourcePort(localPort);
        ack.setDestinationPort(remotePort);
        ack.setWnd(rcv_Queue.free());
        super.network.send(ack);
    }

    // -------------  SEGMENT ARRIVAL  -------------
    @Override
    public void processReceivedSegment(TCPSegment rseg) {
        lock.lock();
        try {
            if (rseg.isAck()) {
                printRcvSeg(rseg);
                int window = rseg.getWnd();
                if (snd_rcvNxt != rseg.getAckNum()) {
                    if (zero_wnd_probe_ON) {
                        zero_wnd_probe_ON = false;
                        log.printBLUE("---ZERO WND PROBE OFF---");
                        stopRTO();
                    }
                    snd_rcvNxt = rseg.getAckNum();
                    stopRTO();
                }
                snd_rcvWnd = window;
                super.appCV.signal();
            } else if (rseg.isPsh()) {
                if (rseg.getSeqNum() != rcv_rcvNxt) {
                    log.printRED("+++++++++ SEGMENT NOT EXPECTED: " + rseg.toString() + " +++++++++");
                    sendAck();
                } else if (!rcv_Queue.full()) {
                    rcv_Queue.put(rseg);
                    printRcvSeg(rseg);
                    rcv_rcvNxt = rseg.getSeqNum() + 1;
                    sendAck();
                    super.appCV.signalAll();
                } else {
                    log.printRED("+++++++++ SEGMENT REJECTED: " + rseg.toString() + " +++++++++");
                    sendAck();
                    super.appCV.signalAll();
                }
            }
        } finally {
            lock.unlock();
        }
    }
}
